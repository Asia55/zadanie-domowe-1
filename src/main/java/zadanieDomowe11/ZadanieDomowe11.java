package zadanieDomowe11;

/**
 * Created by student on 23.05.17.
 */
public class ZadanieDomowe11 {
    public static void main(String[] args) {
        int[] array = new int[4];
        int x = 5;

        int pos = 4 - 1;

        while (x > 0) {
            int mod = x % 2;
            x = x / 2;
            array[pos] = mod;
            pos--;
        }

        for (int i : array) {
            System.out.print(i);
        }

    }
}





