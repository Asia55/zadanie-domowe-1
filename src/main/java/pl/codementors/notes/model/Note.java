package pl.codementors.notes.model;

/**
 * Created by student on 26.05.17.
 */
public class Note {

    private String title;
    private String content;

    public String getTitle() {
        System.out.println();
        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getContent() {

        return content;
    }

    public void setContent(String content) {

        this.content = content;
    }

    public void printToOutput() {
        System.out.println("TITLE: " + title);
        System.out.println("CONTENT: " + content);
    }



}
