package pl.codementors.notes.model;

/**
 * Created by student on 26.05.17.
 */
public class Notepad {

    private Note[] notes = new Note[20];

    public Note getNote(int index) {
        if (index >= 0 && index < notes.length) {
            // Index should be in range [0, notes.length) or [0, notes.length-1].
            return notes[index];
        } else {
            return null;
        }
    }

    public void setNote(int index, Note note) {
        if (index >= 0 && index < notes.length) {
            notes[index] = note;
        }
    }

    public void printNotesToOutput() {

        for (int index = 0; index < notes.length; index++){
            System.out.println("--BEGIN NOTE--");
            System.out.println("INDEX: " + index);

            Note note = notes[index];

            if (note == null) {
                System.out.println("--EMPTY--");
            } else {
                   note.printToOutput();
            }

            System.out.println("--END NOTE--");
        }
        //HOMEWORK: Fill this with proper use of loop, System.out and printToOutput method from note object.
    }


}





